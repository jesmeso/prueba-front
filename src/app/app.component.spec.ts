import { TestBed, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { FormBuilder } from '@angular/forms';
import { AppModule } from './app.module';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent

  const fb: FormBuilder = new FormBuilder();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        AppModule
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });


  it('should create the app', () => {
    expect(component).toBeTruthy();
  });


  it('filter without criteria', () => {
    component.filterForm = fb.group({
      id: null,
      text: null
    });

    component.filter();

    expect(component.gallery.length).toBe(component['galleryService']['NUM_PHOTOS']);
  });


  it('filter by existent ID', () => {
    component.filterForm = fb.group({
      id: 0,
      text: null
    });

    component.filter();

    expect(component.gallery.length).toBe(1);
  });


  it('filter by existent text', () => {
    component.filterForm = fb.group({
      id: null,
      text: 'number 391'
    });

    component.filter();

    expect(component.gallery.length).toBe(11);
  });


  it('filter by inexistent ID', () => {
    component.filterForm = fb.group({
      id: -1,
      text: null
    });

    component.filter();

    expect(component.gallery.length).toBe(0);
  });
  
  
  it('filter by inexistent text', () => {
    component.filterForm = fb.group({
      id: null,
      text: 'qwerty'
    });

    component.filter();

    expect(component.gallery.length).toBe(0);
  });

  
});
