import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { Photo } from 'src/app/models/photo.model';

import { GalleryService } from './services/gallery.service';
import { GalleryFilter } from './models/gallery-filter.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  private _gallery: Array<Photo>;
  public filterForm: FormGroup;

  constructor(private readonly translate: TranslateService, private readonly fb: FormBuilder,
              private readonly galleryService: GalleryService) {
    translate.setDefaultLang('es_ES');
  }

  ngOnInit(): void {
    this.resetFilters();
  }

  public get gallery(): Array<Photo> {
    return this._gallery;
  }

  /**
   * Clear and reset the filter form
   * Fill the gallery with all elements
   */
  public resetFilters(): void {
    this.filterForm = this.fb.group({
      id: [],
      text: []
    });
    this._gallery = this.galleryService.getGallery();
  }

  /**
   * Call the service with the criteria on order to filter elements
   * Gallery is filled with the result
   */
  public filter(): void {
    if (this.filterForm.valid) {
      this._gallery = this.galleryService.getGallery(this.filterForm.value as GalleryFilter);
    }
  }

}
