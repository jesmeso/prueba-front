import { Component, OnInit, Input } from '@angular/core';
import { Photo } from 'src/app/models/photo.model';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html'
})
export class PhotoComponent implements OnInit {

  @Input() photo: Photo;

  constructor() { }

  ngOnInit(): void {
  }



}
