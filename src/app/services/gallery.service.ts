import { Injectable } from '@angular/core';
import { Photo } from '../models/photo.model';
import { GalleryFilter } from '../models/gallery-filter.model';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  private readonly NUM_PHOTOS = 4000;
  private _completeGallery: Array<Photo>;

  constructor() {
    this.setUp();
  }

  /**
   * Initialize the complete gallery with a number of photos equals to NUM_PHOTOS
   */
  private setUp(): void {
    this._completeGallery = new Array<Photo>();

    for (let index = 0; index < this.NUM_PHOTOS; index++) {
      this._completeGallery.push({
        id: index,
        photo: 'https://i.picsum.photos/id/' + index + '/500/500',
        text: 'randomText for photo number ' + index
      });
    }
  }


  /**
   * Return the gallery of photos, if specified, the result will be filtered
   * @param filters optional criteria in order to filter the results.
   *
   * NOTE: This could be more efficient if results were paginated, but it's not what the problem asks for.
   */
  public getGallery(filters?: GalleryFilter): Array<Photo> {
    let result = this._completeGallery;

    if (filters && filters.id !== null) {
      result = result.filter(x => x.id === filters.id);
    }

    if (filters && filters.text) {
      result = result.filter(x => x.text.includes(filters.text));
    }

    return result;
  }
}
