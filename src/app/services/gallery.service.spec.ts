import { TestBed } from '@angular/core/testing';

import { GalleryService } from './gallery.service';

describe('GalleryService', () => {
  let service: GalleryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GalleryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('completeGallery should contain NUM_PHOTOS elements', () => {
    expect(service['_completeGallery'].length).toBe(service['NUM_PHOTOS']);
  });
});
